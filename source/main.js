var cms = angular.module("cms", ['ui.router', 'ngFileUpload', 'angular.filter']);

cms.run(function ($state, $firebase) {
    if(!$firebase.getAuth()){
        $state.go('login');
    }
});

cms.config(function ($stateProvider, $urlRouterProvider) {

    $urlRouterProvider.otherwise("/login");

    $stateProvider.state('login', {
        url: "/login",
        templateUrl: "source/login/login.tpl.html"
    })
    $stateProvider.state('dashboard',{
        url: "/dashboard",
        templateUrl:"source/dashboard/dashboard.tpl.html"
    })
    $stateProvider.state('internacarrera',{
        url: "/carrera",
        templateUrl:"source/interna-carrera/interna-carrera.tpl.html",
        params: {
            idCarrera: null
        }
    })

});


cms.factory('$firebase', function(){
    return new Firebase('https://retogatorade.firebaseio.com');
});

cms.constant('SERVER_IMAGES', 'http://services-anato.rhcloud.com/images.php');