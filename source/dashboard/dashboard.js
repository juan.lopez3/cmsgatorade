var dash = angular.module('cms');

dash.controller('DashCtrl', function($scope, $state, $firebase){
	$scope.carreras = {};
	$scope.fechaActual = new Date();
	//$scope.termsCond = $firebase.child('terminosYCondiciones').;

	$('.ui.checkbox').checkbox();

	$firebase.child('carreraActiva').on('value',function(activeRace){
		$scope.carreraActiva = activeRace.val();
	});
	
	$firebase.child('terminosYCondiciones').on('value',function(termsCond){
		$scope.termsCond = termsCond.val();
	});

	$firebase.child('carreras').on('value',function(carreras){
		$scope.carreras = carreras.val();

		angular.forEach($scope.carreras,function(val, key){
			console.log(key);
			$scope.carreras[key].id = key;
		})

		if ($scope.$root && $scope.$root.$$phase != '$apply' && $scope.$root.$$phase != '$digest')
			$scope.$apply();

	});


	/*Agregar nueva carrera*/
	$scope.addRace = function(){
		$scope.variable = !$scope.variable
		$scope.raceKey = $firebase.child('carreras').push({nombre: $scope.raceName}).then(function($val){
			var raceKey = $val.key();
			console.log('KEY ',raceKey);
			$state.go('internacarrera',{idCarrera: raceKey});
		});
	}

	$scope.borrarCarrera = function(id){
		//Popup de confirmación
		$firebase.child('carreras').child(id).remove();
	}

	$scope.logout = function(){
		$firebase.unauth();
		$state.go('login');
	}

	$scope.activarCarrera = function(carActiva, oKey){
		console.log(carActiva,oKey);
		$firebase.child('carreraActiva').set(oKey);
		$scope.actRace = !$scope.actRace;

	}

	$scope.confirmarAct = function(carAct, nKey){
		if(carAct !== nKey){
			$scope.actRace = !$scope.actRace;
			$scope.newKey = nKey;
		}
	}

	$scope.test = function(){
		$scope.actRace = !$scope.actRace;
	}

	$scope.addTerms = function(){
		$firebase.child('terminosYCondiciones').set($scope.termsCond);
		$scope.terminos = !$scope.terminos;
	}

});

// new Firebase('https://retogatorade.firebaseio.com/carreras').push({nombre: "Nuevo"}).key();
// new Firebase('https://retogatorade.firebaseio.com/carreras').child('-KB8l8EXjfVsqna6ymrt').remove();