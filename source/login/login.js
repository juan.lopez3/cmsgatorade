var login = angular.module('cms');

login.controller('LoginCtrl', function($scope, $state, $firebase){
    $scope.loginfn = function(){

        $firebase.authWithPassword({
            "email": $scope.email,
            "password": $scope.password
        }, function(error, authData) {
            if (error) {
                console.log("Login Failed!", error);
                $scope.msj = "Login Failed!"+error;
            } else {
                console.log("Authenticated successfully with payload:", authData);
                $scope.msj = "Authenticated successfully with payload:"+ authData;
                $state.go('dashboard');
            }
        });
    }
})