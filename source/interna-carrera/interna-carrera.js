var race = angular.module('cms');

race.controller('raceCtrl', function($scope, $state, $stateParams, $firebase, Upload, SERVER_IMAGES){
	if(!$stateParams.idCarrera){
		$state.go('dashboard');
		return;
	}

	$scope.form = {};
	$scope.keyActual = $stateParams.idCarrera;
	$scope.inputValid = true;

	$firebase.child('carreras').child($stateParams.idCarrera).on('value',function(carrera){
		$scope.form = carrera.val();
		$scope.form.date = new  Date($scope.form.date) || new Date();
		$scope.form.hour = new  Date($scope.form.hour) || new Date();
		for(var i in $scope.form.categorias){

			$scope.form.categorias[i].horaInicio = new  Date($scope.form.categorias[i].horaInicio) || new Date();
		}
		if ($scope.$root && $scope.$root.$$phase != '$apply' && $scope.$root.$$phase != '$digest')
			$scope.$apply();

	});

	/*------- Formato campo tiempo estimado ------*/
	$scope.validateHhMm = function(inputField) {
		var isValid = /^([0-1]?[0-9]|2[0-4]):([0-5][0-9])(:[0-5][0-9])?$/.test(inputField);

		if (isValid) {
			console.log("VALID");
			/*inputField.style.backgroundColor = '#bfa';*/
			$scope.inputValid = true;
		} else {
			console.log("NO - VALID");
			$scope.inputValid = false;
			/*inputField.style.backgroundColor = '#fba';*/
		}

		return isValid;
	}

	/*------- Guardar Formulario -------*/
	$scope.saveForm = function(){
		console.dir($scope.form);
		if($scope.inputValid){
			var x = $scope.form;
			var tc = $scope.form.date;
			x.date = $scope.form.date.getTime();
			x.horaInformativa = $scope.form.hour.getTime();

			for(var i in $scope.form.categorias){
				console.log($scope.form.categorias[i].horaInicio.getHours());
				var categoryt = tc;
				categoryt.setHours($scope.form.categorias[i].horaInicio.getHours());
				x.categorias[i].horaInicio = categoryt.getTime();        	
			}

			$firebase.child('carreras').child($scope.keyActual).set(angular.fromJson(angular.toJson(x)));
			$state.go('dashboard');
		}
	}

	/*------- Agregar Categorías -------*/
	$scope.agregarCategoria = function(){

		$firebase.child('carreras').child($scope.keyActual).child('categorias').push({
			horaInicio: "",
			distancia: "",
			tiempos: [{inicio: "", fin: ""},{inicio: "", fin: ""},{inicio: "", fin: ""},{inicio: "", fin: ""}]
		})
	}

	/*------- Eliminar Categorías -------*/
	$scope.eliminarCategoria = function(key){
		$firebase.child('carreras').child($scope.keyActual).child('categorias').child(key).remove();
		/*$scope.form.categorias.splice(key, 1);*/
	}

	/*------- Agregar Checkpoint -------*/
	$scope.agregarCheckPoint = function(cat){
		//console.log('cats',$scope.form.categorias[cat].coordenadas);
		if(!$scope.form.categorias[cat].coordenadas.checkpoints)
			$scope.form.categorias[cat].coordenadas.checkpoints = [];

		$scope.form.categorias[cat].coordenadas.checkpoints.push({
			latitud: "",
			longitud: ""
		});
	}

	/*------- Eliminar Checkpoint -------*/
	$scope.eliminarCheckpoint = function(cat, check){
		$scope.form.categorias[cat].coordenadas.checkpoints.splice(check, 1);
	}

	/*------ Subida Imagenes ----------*/
	$scope.image = function(files){
		if (!files || files[0] == undefined){ return; }

		var file = files[0];
		Upload.upload({
			url: SERVER_IMAGES,
			file: file
		}).progress(function (evt) { })
			.success(function (data, status, headers, config) {
			$scope.form.imagen = data;
			console.log('IMAGEN',data)

		}).error(function (data, status, headers, config) {
			console.log('Error status: ' + status);
		});

	}

	$scope.deleteImg = function () {
		delete $scope.form.imagen;
	};

});
